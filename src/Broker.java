import java.util.ArrayList;
import java.util.List;

public class Broker {
    private List<Commnad> commnadList = new ArrayList<>();

    public void addCommand(Commnad commnad){
        commnadList.add(commnad);
    }

    public void invokeAll(){
        commnadList.stream().forEach(commnad -> {commnad.execute();});
    }
}
